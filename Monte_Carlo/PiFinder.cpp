#include "PiFinder.h"

PiFinder::PiFinder(int numRands) : numRands(numRands) {

}

void PiFinder::makeCalc() {
  // Set up random device + engine
  std::random_device rd;
  std::default_random_engine e(rd());
  std::uniform_real_distribution<> dist(0, 1);

  // Calculations
  double r = 1.0;
  for (int i = 0; i < numRands; i++) {
    double x = dist(e);
    double y = dist(e);
    double distance = x*x + y*y;
    if (distance < 1.0) {
      misses++;
    } else {
      hits++;
    }
  }
}

double PiFinder::getResults() {
  return 4*misses/numRands;
}

void PiFinder::writeResults() {
  std::ofstream out;
  out.open("proc-" + std::to_string(getpid()) + ".out");
  out << std::to_string(getResults());
  out.close();
}


