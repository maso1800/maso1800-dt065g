#include <sys/wait.h>

#include <cmath>
#include "PiFinder.h"
#include <vector>

int main(int argc, char** argv) {
  int numProcs, iterations;
  if (argc != 3) {
    std::cout << "This program expects two parameters: Number of processes, number of iterations." << std::endl;
    return 0;
  } else {
    numProcs = std::stoi(argv[1]);
    iterations = std::stoi(argv[2]);
  }
  std::vector<pid_t> pids(numProcs);

  for (int i = 0; i < pids.size(); i++) {
    if ((pids.at(i) = fork()) < 0) {
      abort();
    } else if (pids.at(i) == 0) {
      PiFinder pf(iterations);
      pf.makeCalc();
      pf.getResults();
      pf.writeResults();
      exit(0);
    }
  }
  
  int status;
  double total = 0; 
  pid_t pid;
  for(int i = 0; i < numProcs; i++) {
    pid = wait(&status);
    if(WIFEXITED(status) && !WEXITSTATUS(status)) {
	std::string name = "proc-" + std::to_string(pid) + ".out";
	std::fstream inf;
	inf.open(name, std::ios::in);
	std::string foo;
        while (getline(inf, foo)) {
          total += std::stof(foo);
          std::cout << "Process " << pid
                    << " exited normally with a return value of " << status
                    << " - PI: " << foo << std::endl;
        }
        std::remove(name.c_str());
    } else if (pid == -1) {
      perror("Failed to wait for process\n");
    } else if (WIFEXITED(status)) {
      std::cout << "Process " << pid << "exited with return status "
		<< WEXITSTATUS(status) << std::endl;
    } else if (WIFSIGNALED(status)) {
      std::cout << "Process " << pid << "terminated due to uncaught signal "
		<< WTERMSIG(status) << std::endl;
    } else if (WIFSTOPPED(status)) {
      std::cout << "Process " << pid << "stopped due to signal "
		<< WSTOPSIG(status) << std::endl;
    }
  }
  std::cout << "Avarage: " << total / numProcs << std::endl;


  return 0;
}
