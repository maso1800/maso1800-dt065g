#include <iostream>
#include <random>
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <string>

#ifndef PiFinderH
#define PiFinderH

class PiFinder {
 private:
  int numRands;
  float hits = 0;
  float misses = 0;
 public:
  PiFinder(int numRands);
  void makeCalc();
  double getResults();
  void writeResults();
};

#endif
